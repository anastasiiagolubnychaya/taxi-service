var autocompletes_depart, autocompletes_arrive, marker, infowindow, map, place_depart, place_arrive, passengers, distance = 0;

function initMap() {
    passengers = document.querySelector('#number_passengers');

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -33.8688, lng: 151.2195},
        zoom: 13
    });
    infowindow = new google.maps.InfoWindow();
    marker = new google.maps.Marker({
        map: map
    });

    var inputs_depart = document.querySelector('#address_depart');
    autocompletes_depart = new google.maps.places.Autocomplete(inputs_depart);

    google.maps.event.addListener(autocompletes_depart, 'place_changed', function () {
        marker.setVisible(false);
        infowindow.close();


        place_depart = autocompletes_depart.getPlace();
        if (!place_depart.geometry) {
            window.alert("Error");
            return;
        }
        if (place_depart.geometry.viewport) {
            map.fitBounds(place_depart.geometry.viewport);
        } else {
            window.alert("Error");
        }

        marker.setIcon(({
            url: place_depart.icon,
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place_depart.geometry.location);
        marker.setVisible(true);

        var address_depart = '';
        if (place_depart.address_components) {
            address_depart = [
                (place_depart.address_components[0] && place_depart.address_components[0].short_name || ''),
                (place_depart.address_components[1] && place_depart.address_components[1].short_name || ''),
                (place_depart.address_components[2] && place_depart.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div style="color: black"><strong>' + place_depart.name + '</strong><br>' + address_depart);
        infowindow.open(map, marker);

        marker.setVisible(false);
        infowindow.close();
        place_depart = autocompletes_depart.getPlace();

        var address_depart = '';
        if (place_depart.address_components) {
            address_depart = [
                (place_depart.address_components[0] && place_depart.address_components[0].short_name || ''),
                (place_depart.address_components[1] && place_depart.address_components[1].short_name || ''),
                (place_depart.address_components[2] && place_depart.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindow.setContent('<div style="color: black"><strong>' + place_depart.name + '</strong><br>' + address_depart);
        infowindow.open(map, marker);
    });

    var inputs_arrive = document.querySelector('#address_arrive');
    autocompletes_arrive = new google.maps.places.Autocomplete(inputs_arrive);

    google.maps.event.addListener(autocompletes_arrive, 'place_changed', function () {
        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setDirections({routes: []});

        place_arrive = autocompletes_arrive.getPlace();

        infowindow.close();

        var directionsService = new google.maps.DirectionsService();


        var request = {
            origin: new google.maps.LatLng(place_depart.geometry.viewport.ma.j, place_depart.geometry.viewport.ga.j), //точка старта
            destination: new google.maps.LatLng(place_arrive.geometry.viewport.ma.j, place_arrive.geometry.viewport.ga.j), //точка финиша
            travelMode: google.maps.DirectionsTravelMode.DRIVING //режим прокладки маршрута
        };

        directionsService.route(request, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
        });

        directionsDisplay.setMap(map);

        var origin = new google.maps.LatLng(place_depart.geometry.viewport.ma.j, place_depart.geometry.viewport.ga.j);
        var destination = new google.maps.LatLng(place_arrive.geometry.viewport.ma.j, place_arrive.geometry.viewport.ga.j);

        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: 'DRIVING'
            }, callback);

        function callback(response, status) {
            if (status == 'OK') {
                distance = response.rows[0].elements[0].distance.value;
            } else {
                window.alert("Ooops! Something went wrong!");
            }
        }
    });

}